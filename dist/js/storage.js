/* global monogatari */

// Persistent Storage Variable
monogatari.storage ({
	player: {
		quiz1: false,
		quiz2: false,
		quiz3: false,
		quiz4: false,
	}
});