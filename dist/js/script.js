/* global monogatari */

// Define the messages used in the game.
monogatari.action ('message').messages ({
	'Help': {
		title: 'Help',
		subtitle: 'Some useful Links',
		body: `
			<p><a href='https://developers.monogatari.io/documentation/'>Documentation</a> - Everything you need to know.</p>
			<p><a href='https://monogatari.io/demo/'>Demo</a> - A simple Demo.</p>
		`
	}
});

// Define the notifications used in the game
monogatari.action ('notification').notifications ({
	'Welcome': {
		title: 'Welcome',
		body: 'This is the Monogatari VN Engine',
		icon: ''
	}
});

// Credits of the people involved in the creation of this awesome game
monogatari.configuration ('credits', {

});

// Define the Particles JS Configurations used in the game
monogatari.action ('particles').particles ({

});

// Define the images that will be available on your game's image gallery
monogatari.assets ('gallery', {

});

// Define the music used in the game.
monogatari.assets ('music', {
	// deepBlue: 'bensound-deepblue.mp3',
	// acousticBreeze: 'bensound-acousticbreeze.mp3',
	// scifi: 'bensound-scifi.mp3',
	// memories: 'bensound-memories.mp3',
	sadDay: 'bensound-sadday.mp3',
	tomorrow: 'bensound-tomorrow.mp3',
	enigmatic: 'bensound-enigmatic.mp3',
	psychedelic: 'bensound-psychedelic.mp3',
	retrosoul: 'bensound-retrosoul.mp3',
	hipjazz: 'bensound-hipjazz.mp3',
	hipjazz_EQ: 'hip jazz_EQ.mp3',
	retrosoul_EQ: 'retro soul_EQ.mp3',
	Magical_Dirt: 'Magical_Dirt.mp3',
	White_Hats: 'White_Hats.mp3',
	creepy: 'bensound-creepy.mp3',
	insects: 'insects.mp3',
	evolution: 'bensound-evolution.mp3',
	november: 'bensound-november.mp3',
	piano_moment: 'bensound-pianomoment.mp3',
	We_ll_Meet_Again: 'We_ll_Meet_Again.mp3',

});

// Define the voice files used in the game.
monogatari.assets ('voices', {});

// Define the sounds used in the game.
monogatari.assets ('sounds', {
	// loudKnock: 'loudKnock.mp3',
	// louderKnock: 'louderKnock.mp3',
	// softKnock: 'softKnock.mp3',
	// cameraShutter: 'cameraShutter.mp3',
	// notification: 'notification.mp3',
	// keyboardLogin: 'keyboardLogin.mp3',
	// slammingTheDoor: 'slammingTheDoor.mp3',
	// openingTheDoor: 'openingTheDoor.mp3',
	// slowDoorOpening: 'slowDoorOpen.mp3',
	schoolBell: 'schoolBell.mp3',
	thump: 'thump.mp3',
	pulling: 'pulling.mp3',
	laughTrack: 'laughTrack.mp3',
	dentalDrill: 'dentalDrill.mp3',
	dentalDrillLong: 'dentalDrillLong.mp3',
	ruriri_007: 'ruriri_007.mp3',
	Glass_Windows_Breaking: 'Glass_Windows_Breaking.mp3',
	bodyfall: 'body fall.mp3',
	bitpowerup: '8-bit-powerup.mp3',
	screeching: 'screeching.mp3',
	doorhandle: 'doorhandle.mp3',
	schoolChildren: 'schoolChildren.mp3',
	hospitalCorridor: 'hospitalCorridor.mp3',
	dentalOffice: 'dentalOffice.mp3',
	slidingDoor: 'slidingDoor.mp3',


});

// Define the videos used in the game.
monogatari.assets ('videos', {

});

// Define the images used in the game.
monogatari.assets ('images', {

});

function canUseWebP () {
	var elem = document.createElement('canvas');
	if (elem.getContext && elem.getContext('2d')) {
		return elem.toDataURL('image/webp').indexOf('data:image/webp') == 0;
	}
	return false;
}
var webPSupported = canUseWebP();

// Define the backgrounds for each scene.
monogatari.assets ('scenes', {
	// bathroom: webPSupported ? 'bathroom.webp' : 'bathroom.png',
	// beachDaySimple: webPSupported ? 'beach_day_simple.webp' : 'beach_day_simple.png',
	// beachDayUmbrella: webPSupported ? 'beach_day_umbrella.webp' : 'beach_day_umbrella.png',
	// beachSunsetSimple: webPSupported ? 'beach_sunset_simple.webp' : 'beach_sunset_simple.png',
	bedroomNight: webPSupported ? 'bedroom_night.webp' : 'bedroom_night.png',
	// bedroom: webPSupported ? 'bedroom.webp' : 'bedroom.png',
	// busStop: webPSupported ? 'bus_stop.webp' : 'bus_stop.png',
	// cafe: webPSupported ? 'cafe.webp' : 'cafe.png',
	// classroomEvening: webPSupported ? 'classroom_evening.webp' : 'classroom_evening.png',
	// classroom: webPSupported ? 'classroom.webp' : 'classroom.png',
	classroomNight: 'classroom_night.png',
	// hallwayHouse: webPSupported ? 'hallway_house.webp' : 'hallway_house.jpg',
	// hallwayHouseNight: webPSupported ? 'hallway_house_night.webp' : 'hallway_house_night.png',
	// hallwaySchool: webPSupported ? 'hallway_school.webp' : 'hallway_school.png',
	hallwaySchoolNight: 'hallway_school_night.png',
	hallwaySchoolDarker30: 'hallway_school_darker_30.png',
	hallwaySchoolDarker50: 'hallway_school_darker_50.png',
	hallwaySchoolDarker60: 'hallway_school_darker_60.png',
	hallwaySchoolDarker70: 'hallway_school_darker_70.png',
	hallwaySchoolDarker75: 'hallway_school_darker_75.png',
	hallwaySchoolDarker80: 'hallway_school_darker_80.png',
	hallwaySchoolDarker90: 'hallway_school_darker_90.png',
	// hallway: webPSupported ? 'hallway.webp' : 'hallway.jpg',
	// hallway2: webPSupported ? 'hallway2.webp' : 'hallway2.jpg',
	// hallwayNight: webPSupported ? 'hallway_night.webp' : 'hallway_night.jpg',
	// housesEvening: webPSupported ? 'houses_evening.webp' : 'houses_evening.png',
	// housesNight: webPSupported ? 'houses_night.webp' : 'houses_night.png',
	// houses: webPSupported ? 'houses.webp' : 'houses.png',
	// japaneseSetting: webPSupported ? 'japanese_setting.webp' : 'japanese_setting.png',
	// kitchen: webPSupported ? 'kitchen.webp' : 'kitchen.png',
	// livingRoom: webPSupported ? 'living_room.webp' : 'living_room.png',
	// livingRoomNight: webPSupported ? 'living_room_night.webp' : 'living_room_night.png',
	// moonLinkFence: webPSupported ? 'moon_link_fence.webp' : 'moon_link_fence.png',
	// moonOverField: webPSupported ? 'moon_over_field.webp' : 'moon_over_field.png',
	// moonPark: webPSupported ? 'moon_park.webp' : 'moon_park.png',
	// moon: webPSupported ? 'moon.webp' : 'moon.png',
	// park: webPSupported ? 'park.webp' : 'park.png',
	// schoolCafeteria: webPSupported ? 'school_cafeteria.webp' : 'school_cafeteria.png',
	// schoolEntranceEvening: webPSupported ? 'school_entrance_evening.webp' : 'school_entrance_evening.png',
	// schoolEntrance: webPSupported ? 'school_entrance.webp' : 'school_entrance.png',
	// schoolRooftop: webPSupported ? 'school_rooftop.webp' : 'school_rooftop.png',
	// stairs: webPSupported ? 'stairs.webp' : 'stairs.png',
	// kitchenNight: webPSupported ? 'kitchen_night.webp' : 'kitchen_night.png',
	// end: webPSupported ? 'end.webp' : 'end.png',
	// Visuals 02 starts here
	traditionalPavilion: 'traditional_pavilion.png',
	traditionalPavilionNight: 'traditional_pavilion_night.png',
	street1: 'street_1.png',
	street_1Night: 'street_1Night.png',
	street2: 'street_2.png',
	shop1: 'shop_1.png',
	resto: 'resto.png',
	nightDistrict1: 'night_district_1.png',
	nightDistrict2: 'night_district_2.png',
	mountFuji: 'mount_fuji.png',
	japaneseDinner: 'japanese_dinner.png',
	houseNight: 'houseNight.png',
	houseOld: 'house_old.png',
	houseOldNight: 'house_old_night.png',
	cityView: 'city_view.png',
	time: 'time.jpg',
	dentalOffice: 'dental_office.png',
});


// Define the Characters
monogatari.characters ({
	j: {
		name: 'Jessie',
		color: '#f94e5c',
		directory: 'jessie',
		sprites :{
			jpose1scared: 'jpose1scared.png',
			jpose1bored: 'jpose1bored.png',
			jpose1neutral: 'jpose1neutral.png',
			jpose1thinking: 'jpose1thinking.png',
			jpose1cheeky: 'jpose1cheeky.png',
			jpose1annoyed: 'jpose1annoyed.png',
			jpose1surprised: 'jpose1surprised.png',
			jpose1relieved: 'jpose1relieved.png',
			jpose2scared: 'jpose2scared.png',
			jpose2emberrassed: 'jpose2emberrassed.png',
			jpose2disgusted: 'jpose2disgusted.png',
			jpose3determined: 'jpose3determined.png',
			jpose3surprised: 'jpose3surprised.png',
			jpose3determinedsmile: 'jpose3determinedsmile.png',
			jpose3veryangryscreamo: 'jpose3veryangryscreamo.png',
			jpose3veryangrycrying: 'jpose3veryangrycrying.png',
			jpose3crying: 'jpose3crying.png',
			jpose3cryingsmile: 'jpose3cryingsmile.png',
		}
	},
	f: {
		name: 'Frankie',
		color: '#00bfff',
		directory: 'frankie',
		sprites :{
			fpose1smile: 'fpose1smile.png',
			fpose1smileclosedeyes: 'fpose1smileclosedeyes.png',
			fpose1creepy: 'fpose1creepy.png',
			fpose1cheeky: 'fpose1cheeky.png',
			fpose1neutral: 'fpose1neutral.png',
			fpose2excited: 'fpose2excited.png',
			fpose2surprised: 'fpose2surprised.png',
			fpose2fakecry: 'fpose2fakecry.png',
			fpose2creepy: 'fpose2creepy.png',
			fpose2angry: 'fpose2angry.png',
			fpose2determined: 'fpose2determined.png',
			fpose3neutral: 'fpose3neutral.png',
			fpose3bored: 'fpose3neutral.png',
			fpose3creepy: 'fpose3creepy.png',
			fpose3sad: 'fpose3sad.png',
			fpose3emberrassed: 'fpose3emberrassed.png',
			fpose3disgusted: 'fpose3disgusted.png',
			fpose3serious: 'fpose3serious.png',
			fpose3crying: 'fpose3crying.png',
			fpose3cryingsmile: 'fpose3cryingsmile.png',
		}
	},
	'f?': {
		name: '???',
		color: '#00bfff',
	},
	p: {
		name: 'Pszemek',
		color: '#c4c4c4',
		directory: 'npc',
		sprites :{
			ppose1neutral: 'ppose1neutral.png'
		}
	},
	n: {
		name: 'Nauczycielka',
		color: '#c4c4c4',
		directory: 'npc',
		sprites :{
			npose1neutral: 'npose1neutral.png',
		}
	},
	d: {
		name: 'Dentysta',
		color: '#c4c4c4',
		directory: 'npc',
		sprites :{
			dpose1neutral: 'dpose1neutral.png',
		}
	},
	jf: {
		name: 'Jessie i Frankie',
		color: '#f901f0',
	},
	todo: {
		name: 'TODO!',
		color: '#f9b000',
	}
});

function playVoice (actor, index) {
	return `play voice ${actor}${String(index).padStart(3, '0')}.mp3`;
	// return function () {return true;};
}

var characters = {
	f: 'aloes_',
	'f?': 'aloes_',
	j: 'ruriri',
	jf: 'jf_',
};

var charactersThink = {
	f: 't-aloes',
	j: 't-ruriri',
};

function reversibleTwoWay (func) {
	return {'Function':{
		'Apply': function () {
			return func();
		},
		'Reverse': function () {
			return func();
		}
	}};
}

function say (character, index, text) {
	return reversibleTwoWay(function () {
		monogatari.run('stop voice', false).then(function () {
			if (index) {
				return monogatari.run(playVoice(characters[character] ? characters[character] : character, index), false);
			}
		});
		monogatari.run(`${character} ${text}`, false);
		return false;
	});
}

function think (character, index, text) {
	return reversibleTwoWay(function () {
		monogatari.run('stop voice', false).then(function () {
			if (index) {
				return monogatari.run(playVoice(charactersThink[character] ? charactersThink[character] : character, index), false);
			}
		});
		monogatari.run(`${character} <em>${text}</em>`, false);
		return false;
	});
}

function sayRandom (dialogs) {
	return reversibleTwoWay(function () {
		var dialogIndex = Math.floor((Math.random() * dialogs.length));
		var randomDialog = dialogs[dialogIndex];
		var character = randomDialog.character;
		var index = randomDialog.index;
		var text = randomDialog.text;
		monogatari.run('stop voice', false).then(function () {
			if (index) {
				return monogatari.run(playVoice(characters[character] ? characters[character] : character, index), false);
			}
		});
		monogatari.run(`${character} ${text}`, false);
		return false;
	});
}



monogatari.script ({
	Start: [
		'play music hipjazz with loop',
		'show scene bedroomNight',
		'show character j jpose1neutral at center with fadeIn',
		think('j', 1, 'Kolejny nudny dzień za mną...'),
		think('j', 2, 'Na matmie znowu byłem najlepszy. Serio, reszta tych dzieciaków to jacyś idioci.'),
		think('j', 3, 'Może dlatego nikt nie chce ze mną gadać... nie żeby mnie to jakoś specjalnie obchodziło.'),
		'show character j jpose1bored at center',
		'...',
		'play sound ruriri_007',
		'wait 5000',
		think('j', 4, 'Ech, na dole znowu się kłócą...'),
		think('j', 5, 'Pewnie siostra znowu wróciła 3 minuty za późno do domu.'),
		'show character j jpose1annoyed at center',
		say('j', 1, 'Jest 22, uciszcie się już! Niektórzy próbują spać!'),
		'show character j jpose1neutral at center',
		think('j', 6, 'I tak nie jest mi łatwo...'),
		think('j', 7, 'Wstyd przyznać, ale mimo że mam już 12 lat, nadal czasem boję się zasnąć.'),
		think('j', 8, 'Sen to taki zgon w wersji demo i wcale mi się to nie podoba.'),
		think('j', 9, 'Jak będę chciał umrzeć, to po prostu umrę, nie potrzebuję półśrodków.'),
		think('j', 10, 'Zresztą kiedy rozum śpi, budzą się demony...'),
		think('j', 11, 'Szczególnie te pod łóżkiem, aż strach czasem wystawić nogę poza kołdrę.'),
		say('j', 2, '*zieeew*'),
		'stop music tomorrow with fade 10',
		think('j', 12, 'Ciekawe z czym mama jutro zrobi kanapki...'),
		think('j', 13, 'Może z dżeemem... może z serem żółtym...'),
		think('j', 14, 'Zjem wszystko, byle nie...'),
		'show background black with fadeIn duration 5s',
		'stop music hipjazz with fade 5',
		'clear',
		'hide character j with fadeOut',
		'wait 3000',
		'play music hipjazz_EQ with loop',
		'show background time with fadeIn',
		say('j', 3, 'Huh? Co się dzieje?'),
		think('j', 15, 'Coś zimnego trzyma mnie za nogę!'),
		think('j', 16, 'Chyba ciągnie mnie w dół...'),
		'stop music hipjazz_EQ with fade 5',
		'play sound bodyfall',
		'show scene bedroomNight',
		'show character j jpose3veryangryscreamo at center with fadeIn',
		say('j', 4, 'Aggh! Co jest?!'),
		say('j', 5, 'Spadłem z łóżka? Nie, chwila...'),
		'show character j jpose2scared at center',
		say('j', 6, 'Naprawdę coś trzyma mnie za no-! *krzyk*'),
		'play sound pulling',
		'play sound Glass_Windows_Breaking',
		'jump chapter1'
	],
	chapter1: [
		'show scene black with fadeIn',
		'wait 5000',
		say('j', 7, 'Mmmm...'),
		'play music enigmatic with loop',
		say('f?', 1, 'Obudzisz się w końcu czy działa na ciebie tylko rypnięcie w podłogę?'),
		'show scene traditionalPavilionNight with fadeIn',
		'show character j jpose2scared at leftish with fadeIn',
		'show character f fpose1smile at rightish with fadeIn',
		say('j', 8, 'Agh?!'),
		'show character f fpose1smileclosedeyes at rightish',
		say('f?', 2, 'No, w kooońcu, śpiąca królewno! Jak długo można czekać?'),
		'Miejsce, w którym się teraz znajdowałem, w ogóle nie przypominało mojego pokoju, a osoby, która stała obok mnie, nigdy wcześniej nie widziałem na oczy.',
		say('j', 9, 'Co jest? Gdzie ja jestem?'),
		'show character f fpose1smile at rightish',
		say('f?', 3, 'Jak to gdzie? Jesteś w moim domu. Wytarłbyś buty.'),
		think('j', 17, 'Buty?'),
		'show character j jpose1annoyed at leftish',
		say('j', 10, 'K-kim ty jesteś?'),
		'show character f fpose3bored at rightish',
		say('f?', 4, 'Ech, czy to jest moment, w którym przechodzimy do nudnej prezentacji świata?'),
		'show character f fpose2excited at rightish',
		say('f?', 5, 'Nie! Kogo to obchodzi?!'),
		say('f?', 6, 'Nie ściągnąłem cię tutaj, żebyś zadawał durne pytania!'),
		say('j', 11, 'Durne pytania? A na co ci potrzebny dwunastoletni chłopiec?'),
		'show character f fpose1cheeky at rightish',
		say('f?', 7, 'A jak myślisz?'),
		'show character j jpose2disgusted at leftish',
		'j ...',
		'show character f fpose1smileclosedeyes at rightish',
		say('f?', 8, 'Głodnemu chleb na myśli! Hihi!'),
		'show character f fpose1smile at rightish',
		say('f?', 9, 'No doobra, prawda jest taka, że ściągnąłem cię... bo mi się nudziło!'),
		'show character j jpose1bored at leftish',
		say('j', 12, 'Co za beznadziejny powód.'),
		say('j', 13, 'I co, będziesz kazał mi questy robić?'),
		'show character f fpose1neutral at rightish',
		say('f?', 10, '...'),
		'show character f fpose1smileclosedeyes at rightish',
		say('f?', 11, 'Zgadza się! Z ust mi to wyjąłeś!'),
		say('f?', 12, 'Dokładnie to tutaj robimy! Ee... questy robimy!'),
		say('j', 14, 'Super. Chcę do domu.'),
		say('f?', 13, 'I właśnie dlatego będziesz je robił! Hihi!'),
		'stop music enigmatic',
		'show character f fpose1creepy at rightish',
		say('f?', 14, 'Bez nich nigdy się stąd nie wydostaniesz i zostaniesz tu na zawsze.'),
		'show character j jpose2scared at leftish',
		say('j', 15, 'Huh?!'),
		'play music enigmatic with loop',
		'show character f fpose2surprised at rightish',
		say('f?', 15, 'Ojej, przestraszyłem cię?'),
		'show character j jpose1bored at leftish',
		say('j', 16, 'Twoja fryzura przestraszyła mnie bardziej, jak cię zobaczyłem.'),
		'show character f fpose2fakecry at rightish',
		say('f?', 16, 'No wiesz co, ale jesteś niemiły! Przecież sam mnie takiego stworzyłeś...'),
		'show character j jpose1annoyed at leftish',
		say('j', 17, 'Co?'),
		say('f?', 17, 'Nie poznajesz mnie?'),
		'stop music enigmatic',
		'show character f fpose2creepy at rightish',
		'show character j jpose2scared at leftish',
		say('f?', 18, 'To przecież mnie widzisz codziennie w koszmarach!'),
		say('f?', 19, 'To mnie bałeś się od najmłodszych lat!'),
		'play music enigmatic with loop',
		'show character f fpose1smileclosedeyes at rightish',
		say('f?', 20, 'W końcu... to przeze mnie boisz się wystawić nogę poza kołdrę, prawda?'),
		say('j', 18, 'Jesteś... potworem spod łóżka...?'),
		say('j', 19, 'To ty mnie tu wciągnąłeś!'),
		'show character f fpose2excited at rightish',
		say('f?', 21, 'Ojej, czyżbyś przejrzał mój ultrasekretny sekret?'),
		say('f?', 22, 'Tak! To ja!'),
		'show character f fpose2fakecry at rightish',
		say('f?', 23, 'Jesteś zły? Proszę, nie bądź zły...'),
		'show character f fpose3bored at rightish',
		say('f?', 24, 'A w sumie bądź, średnio mnie to obchodzi.'),
		'show character j jpose1thinking at leftish',
		think('j', 18, 'To wszystko nie ma sensu... dlaczego tak nagle się tu znalazłem? I gdzie jest „tu”...?'),
		'show character j jpose1neutral at leftish',
		say('j', 20, 'Czy to wszystko... znajduje się pod moim łóżkiem?'),
		'show character f fpose2excited at rightish',
		say('f?', 25, 'Bingo! Ten caluuuutki świat został stworzony dla ciebie!'),
		'show character f fpose3bored at rightish',
		say('f?', 26, 'No, przynajmniej część, w której jesteśmy.'),
		say('f?', 27, 'Nie jesteś jedynym dzieckiem, które boi się potwora spod łóżka. Nie myśl, że jesteś wyjątkowy.'),
		'show character j jpose1thinking at leftish',
		think('j', 19, 'Nie jestem jedyny? Czyli każdy dzieciak ma coś takiego pod swoim łóżkiem?! Dlaczego w takim razie nigdy o czymś takim nie słyszałem?'),
		'show character j jpose1neutral at leftish',
		say('j', 21, 'Czy to normalne dla potwora spod łóżka, by ściągać do siebie dziecko, które straszy?'),
		'show character f fpose3creepy at rightish',
		'show character j jpose2scared at leftish',
		'stop music enigmatic',
		say('f?', 28, 'A co cię to obchodzi?'),
		say('f?', 29, 'Nie przypominam sobie, żebym musiał odpowiadać na każde twoje pytanie.'),
		'play music enigmatic with loop',
		'show character f fpose1smileclosedeyes at rightish',
		say('f?', 30, 'Czy to wszystkie superwkurzające rzeczy, o które chciałeś mnie zapytać?'),
		'show character j jpose1annoyed at leftish',
		say('j', 22, 'I tak byś mi nic nie powiedział, ee...'),
		say('j', 23, 'Jak się właściwie nazywasz?'),
		say('j', 24, 'Skoro spędzę tu z tobą najgorsze chwile swojego życia, wykonując durne questy, to przynajmniej to powinienem wiedzieć.'),
		'show character f fpose3bored at rightish',
		say('f?', 31, 'Ach, imiona?'),
		say('f?', 32, 'No tak, ludzie z jakiegoś powodu bardzo lubią mieć własne.'),
		say('f?', 33, 'Sorki, ale nikt z nas ich tutaj nie potrzebuje!'),
		'show character j jpose1neutral at leftish',
		think('j', 20, 'Nikt z nas?'),
		say('j', 25, 'W takim razie będziesz Frankie.'),
		'show character f fpose2excited at rightish',
		say('f', 34, 'Oooch, czy to imię osoby, która ci się podoba? Nazwałeś mnie po niej! Och, przestań, bo się zarumienię...'),
		'show character j jpose1bored at leftish',
		say('j', 26, 'Mój pies się tak nazywał, zanim zdechł. Był tak irytująco głośny, jak ty.'),
		'show character f fpose1neutral at rightish',
		say('f', 35, '...'),
		say('j', 27, 'Idziemy czy nie? Nie mam całego dnia.'),
		'stop music enigmatic with fade 3',
		'jump chapter2'
	],
	chapter2 : [
		'show scene black with fadeIn',
		'wait 1000',
		'show scene nightDistrict1 with fadeIn',
		'show character j jpose1neutral at leftish with fadeIn',
		'show character f fpose1smile at rightish with fadeIn',
		'play music psychedelic with loop',
		say('j', 28, 'No więc? Na czym mają polegać te questy?'),
		say('f', 36, 'A, nic wielkiego, musisz tylko odwiedzić paru starych przyjaciół!'),
		'show character j jpose1surprised at leftish',
		say('j', 29, 'Starych przyjaciół?'),
		'show character f fpose1cheeky at rightish',
		say('f', 37, 'Jak tylko ich zobaczysz, będziesz wiedział, o czym mówię!'),
		'show character f fpose1smileclosedeyes at rightish',
		say('f', 38, 'O, o wilku mowa!'),
		'stop music psychedelic with fade 5',
		'show character j jpose2scared at leftish',
		'Nagle poczułem czyjąś dłoń na swoim ramieniu i przeszedł mnie zimny dreszcz.',
		'Czułem wielką chęć, by uciekać, ale nogi odmówiły mi posłuszeństwa...',
		'show character p ppose1neutral at tower with fadeIn',
		'play music White_Hats with loop',
		'p Cześć, chłopcze.',
		'p Wyglądasz, jakbyś zabłądził, potrzebujesz pomocy?',
		'p Nie powinieneś błąkać się tu o takiej godzinie, szczególnie w takim towarzystwie.',
		'show character f fpose3neutral at leftish',
		say('f', 39, '...'),
		'p Chętnie pomogę ci znaleźć drogę do domu.',
		'show character j jpose2disgusted at leftish',
		think('j', 21, 'Ten typ na pewno nie jest godny zaufania.'),
		think('j', 22, 'Z drugiej strony, skąd mogę wiedzieć, że Frankie mówi prawdę?'),
		{'Choice': {
			'chapter2.1': {
				'Text': 'Tak, zabierz mnie do domu.',
				'Do': 'jump chapter2a'
			},
			'chapter2.2': {
				'Text': 'Eee, nie jestem przekonany...',
				'Do': 'jump chapter2a'
			}
		}}
	],

	chapter2a: [
		'show character j jpose1cheeky at leftish',
		'stop music White_Hats',
		say('j', 30, 'Spadaj na bambus, frajerze!'),
		'show character j jpose1scared at leftish',
		'play music psychedelic',
		say('j', 31, 'Co, chwila, nie to chciałem powiedzieć!'),
		'show character f fpose1cheeky at rightish',
		say('f', 40, 'Hihi! Język ci się plącze?'),
		'show character j jpose1thinking at leftish',
		think('j', 23, 'Czy to jakaś sztuczka Frankiego?'),
		'p Oj, nie daj się prosić... mam cukierki, ciastka i małe kotki w piwnicy!',
		{'Choice': {
			'chapter2a.1': {
				'Text': 'Daj tego cukierka!',
				'Do': 'jump chapter2b'
			},
			'chapter2a.2': {
				'Text': 'Nie jem słodyczy i nie lubię kotów.',
				'Do': 'jump chapter2b'
			}
		}}
	],

	chapter2b: [
		'show character j jpose1cheeky at leftish',
		say('j', 32, 'Te cukierki to pewnie z glutenem... żałosne.'),
		'show character j jpose3veryangryscreamo at leftish',
		say('j', 33, 'Frankie, wiem, że to twoja sprawka!'),
		'show character f fpose2excited at rightish',
		say('f', 41, 'Ojej, skapnąłeś się? Hihi, przepraszam, już nie będę!'),
		'show character j jpose1thinking at leftish',
		think('j', 24, 'Niedobrze, jak tak dalej pójdzie, to się zdenerwuje i serio mi coś zrobi!'),
		think('j', 25, 'Mama zawsze ostrzegała mnie przed takimi typami...'),
		think('j', 26, 'Gdybym tylko potrafił go spławić...'),
		'p Za rogiem stoi moja furgonetka z lodami... Może chcesz jednego za darmo?',
		{'Choice': {
			'chapter2b.1': {
				'Text': 'Tak, chętnie!',
				'Do': 'jump chapter2c'
			},
			'chapter2b.2': {
				'Text': 'To brzmi dwuznacznie.',
				'Do': 'jump chapter2c'
			}
		}}
	],

	chapter2c: [
		'show character j jpose1bored at leftish',
		say('j', 34, 'Sorry, ale wyglądasz, jakbyś miał sądowy zakaz zbliżania się do przedszkoli.'),
		'show character j jpose1scared at leftish',
		say('j', 35, 'Frankie, to nie jest śmieszne!'),
		say('j', 36, 'On nie wygląda na kogoś, z kogo można żartować!'),
		'show character f fpose3neutral at rightish',
		say('f', 42, 'Pff, nie wiesz, co to dobra zabawa.'),
		'show character j jpose1thinking at leftish',
		think('j', 27, 'Nie mogę dłużej tego ciągnąć, muszę wymyślić, jak go spławić...'),
		think('j', 28, 'Może nie muszę powiedzieć niczego szczególnego... Może wystarczy...'),
		'p Oj, mój mały, grabisz sobie! Chodź ze mną, pokażę ci, czym jest dobra zabawa!',
		'stop music psychedelic',
		{'Choice': {
			'chapter2c': {
				'Text': 'Nie, zostaw mnie w spokoju!',
				'Do': 'jump chapter2d'
			}
		}}
	],

	chapter2d: [
		'show character j jpose3determined at leftish',
		say('j', 37, 'Nie! Zostaw mnie w spokoju!'),
		'Nogi ugięły się pode mną, a plecy miałem mokre od potu.',
		'hide character p with fadeOut',
		'p ...',
		'show character j jpose3surprised at leftish',
		say('j', 38, 'C-co...? Gdzie on się podział?'),
		'play music enigmatic with loop',
		'show character f fpose3neutral at rightish',
		say('f', 43, 'Nigdzie, już nie jest nam potrzebny.'),
		'show character f fpose2excited at rightish',
		say('f', 44, 'Spójrz, zostawił coś dla ciebie!'),
		'W miejscu, w którym przed chwilą stał pan totalnie-nie-pedofil, teraz na ziemi pojawił się cukierek owinięty w kolorową folię.',
		'show character j jpose2disgusted at leftish',
		say('j', 39, 'Fuj, kto wie, co jest w tym cukierku.'),
		say('j', 40, 'Brzydzę się nawet go dotknąć, nie mówiąc o jedzeniu.'),
		'show character f fpose1smile at rightish',
		say('f', 45, 'Jak to, nie weźmiesz swojej nagrody za zrobienie questa?'),
		say('f', 46, 'Jeśli się nie pospieszysz, ktoś zaraz sprzątnie ci go sprzed nosa!'),
		'show character j jpose1thinking at leftish',
		think('j', 29, 'Może rzeczywiście lepiej, żeby nikt go przez przypadek nie wziął...'),
		'play sound bitpowerup',
		say('f', 47, 'Dodano cukierek do ekwipunku!'),
		'show character f fpose1smileclosedeyes at rightish',
		say('f', 48, 'I co, chyba nie było tak strasznie?'),
		'show character j jpose1cheeky at leftish',
		say('j', 41, 'Zabrzmiało prawie, jakbyś się o mnie martwił.'),
		'stop music enigmatic',
		'show character f fpose1creepy at rightish',
		'show character j jpose1scared at leftish',
		say('f', 49, 'Byłoby niedobrze, gdyby coś ci się stało.'),
		'play music enigmatic with loop',
		'show character f fpose1smileclosedeyes at rightish',
		say('f', 50, 'Prawda?'),
		'show character j jpose1neutral at leftish',
		'j ...',
		say('j', 42, 'To mogę teraz wrócić do domu?'),
		say('f', 51, 'No coś ty! Zabawa dopiero się zaczyna!'),
		'jump chapter3',
	],

	chapter3: [
	 	'play sound schoolBell',
		'wait 2500',
		 'show character f fpose2excited at rightish',
		say('f', 52, 'O, zresztą sam słyszysz! Chodź, chyba nie chcesz się spóźnić na lekcje! Przecież kooochasz szkołę!'),
		'show character j jpose1bored at leftish',
		say('j', 43, 'Ta, w szczególności tych wszystkich geniuszy, którzy tam chodzą...'),
		'show character f fpose1smileclosedeyes at rightish',
		say('f', 53, 'Jestem przekonany, że oni na pewno kochają cię równie mocno!'),
		say('f', 54, 'Twój snobizm jest naprawdę ujmujący!'),
		'j ...',
		'show background houseNight with fadeIn',
		'Frankie poprowadził mnie w stronę dużej kamienicy, wyróżniającej się rozmiarem spośród okolicznych budynków.',
		'Poczułem nieprzyjemny dreszcz, jak zawsze przed wejściem na pierwszą lekcję.',
		'stop music enigmatic with fade 5',
		'show background hallwaySchoolNight with fadeIn',
		'play sound schoolChildren with loop',
		'show character j jpose1thinking at leftish',
		think('j', 30, 'Głośno tu, zupełnie jak w normalnej szkole. Tylko gdzie są wszyscy?'),
		think('j', 31, 'Słyszę, jak krzyczą tuż obok mnie, ale nikogo nie widzę...'),
		say('f', 55, 'Ach, uwielbam odgłosy cierpienia o poranku.'),
		'show character f fpose1creepy at rightish',
		'show character j jpose1surprised at leftish',
		'stop sound schoolChildren',
		say('f', 56, 'Niedługo do nich dołączysz!'),
		'show character f fpose1cheeky at rightish',
		'play sound schoolChildren with loop',
		say('f', 57, 'A tymczasem chodźmy, wszyscy czekają już tylko na ciebie!'),
		'show background classroomNight with fadeIn',
		'show character j jpose1thinking at leftish',
		think('j', 32, 'Klasa też wydaje się pusta, ale czuję, jakby obserwowało mnie 20 osób.'),
		think('j', 33, 'Może jeśli usiądę w ostatniej ławce, też będę w stanie wtopić się w tłum?'),
		'show character n npose1neutral at tower with fadeIn',
		'stop sound schoolChildren with fade 5',
		'play sound screeching',
		'n Gdzie się wybierasz, młody człowieku?',
	 	'play music creepy with loop',
		'show character j jpose2scared at leftish',
		'n Do tablicy! Dzisiaj twoja kolej!',
		'show character j jpose2disgusted at leftish',
		think('j', 34, 'Okej, tamten typek był obleśny, ale chociaż udawał miłego. Ta wygląda na skończoną jędzę.'),
		think('j', 35, 'Brr, i do tego patrzy się na mnie tymi oczami, jakby przeszywała mnie na wylot. Obrzydliwe.'),
		'show character f fpose1cheeky at rightish',
		say('f', 58, 'Co, myślałeś, że się schowasz? Przecież mówiłem, że zabawa trwa dalej!'),
		'show character j jpose1annoyed at leftish',
		think('j', 36, 'A Frankie rozsiadł się jak królewicz w pierwszej ławce i myśli, że może mnie pouczać.'),
		think('j', 37, 'Frajer.'),
		think('j', 38, 'Debil.'),
		say('j', 44, 'Idiota.'),
		'show character f fpose1neutral at rightish',
		say('f', 59, 'Mówiłeś coś?'),
		say('j', 45, 'Mam nadzieję, że tym razem nie będziesz wtykać nosa w nie swoje sprawy.'),
		'show character f fpose2fakecry at rightish',
		say('f', 60, 'Jak to nie moje sprawy? Jesteś moim najlepszym przyjacielem! Musimy sobie pomagać!'),
		'show character j jpose3determined at leftish',
		say('j', 46, 'Do dupy z takim przyjacielem!'),
		'n Jak się odzywasz do kolegi?! Podejdziesz do tablicy czy potrzebujesz specjalnego zaproszenia?',
		'play sound laughTrack',
		'show character f fpose1cheeky at rightish',
		'show character j jpose2emberrassed at leftish',
		'n Ostatnio się ośmieszyłeś, ale może tym razem lepiej ci pójdzie. Chociaż nie mam wobec ciebie zbyt wysokich oczekiwań. Ktoś tak tępy jak ty trafia się w tej szkole raz na kilkadziesiąt lat.',
		'Jej wyśmiewanie się ze mnie przypomniało mi o tych wszystkich razach, kiedy skompromitowałem się w szkole.',
		'O tym, jak w czwartej klasie byłem pewny, że znam odpowiedź, ale powiedziałem kompletną głupotę i śmiała się ze mnie cała klasa.',
		'Albo o tym, jak przez przypadek powiedziałem do nauczycielki „mamo”.',
		'Na samą myśl o tych wydarzeniach miałem ochotę wybiec z klasy i schować się w szkolnej łazience.',
		'n Zacznijmy od czegoś prostego, z czym każdy byłby w stanie sobie poradzić.',
		'play sound laughTrack',
		'Jej słowa brzmiały jak zachęta, ale w głosie słychać było kpinę.',
		'Zacisnąłem dłonie w pięści tak mocno, że czułem, jak paznokcie kaleczą mi ręce.',
		'show character j jpose1thinking at leftish',
		think('j', 39, 'Patrząc na to, co do tej pory mnie spotkało, nie ma mowy, że to pytanie będzie rzeczywiście proste.'),
		'Serce biło mi jak szalone, jak zawsze podczas odpytywania.',
		'Przygotowywałem się na najgorsze. Oczyma wyobraźni widziałem już jej triumfalny uśmieszek, kiedy całkowicie się kompromituję.',
		'show character j jpose1scared at leftish',
		think('j', 40, 'A co jeśli w tym świecie za złą odpowiedź nie mi grozi jedynie słaba ocena?'),
		think('j', 41, 'Ten wskaźnik, który trzyma w ręku, jest naprawdę ostry.'),
		think('j', 42, 'Co jeśli naprawdę stanę się jednym z tych niewidzialnych głosów?'),
		'Wstrzymałem oddech, kiedy otworzyła usta, żeby zadać mi pytanie.',
		'n Matematyka. Ile to będzie sześć razy siedem?',
		'show character f fpose2excited at rightish',
		say('f', 61, '69!'),
		'show character j jpose1neutral at leftish',
		'Ze świstem wypuściłem powietrze.',
		think('j', 43, 'Może jednak dam sobie radę.'),
		'jump quiz1',
	],

	quiz1fail: [
		'show character f fpose1cheeky at rightish',
		sayRandom([
			{character: 'f', index: 62, text: 'Ha, i kto tu jest idiotą!'},
			{character: 'f', index: 64, text: 'Pewnie zamiast się uczyć, nagrywałeś fanduby.'},
			{character: 'f', index: 65, text: 'Twoja stara musiała zjeść dużo lodu, żeby urodzić takiego bałwana.'},
		]),
		'play sound laughTrack',
		'jump quiz1',
	],

	quiz2fail: [
		'show character f fpose1cheeky at rightish',
		sayRandom([
			{character: 'f', index: 62, text: 'Ha, i kto tu jest idiotą!'},
			{character: 'f', index: 64, text: 'Pewnie zamiast się uczyć, nagrywałeś fanduby.'},
			{character: 'f', index: 65, text: 'Twoja stara musiała zjeść dużo lodu, żeby urodzić takiego bałwana.'},
		]),
		'play sound laughTrack',
		'jump quiz2',
	],

	quiz3fail: [
		'show character f fpose1cheeky at rightish',
		sayRandom([
			{character: 'f', index: 62, text: 'Ha, i kto tu jest idiotą!'},
			{character: 'f', index: 64, text: 'Pewnie zamiast się uczyć, nagrywałeś fanduby.'},
			{character: 'f', index: 65, text: 'Twoja stara musiała zjeść dużo lodu, żeby urodzić takiego bałwana.'},
		]),
		'play sound laughTrack',
		'jump quiz3',
	],

	quiz4fail: [
		'show character f fpose1cheeky at rightish',
		sayRandom([
			{character: 'f', index: 62, text: 'Ha, i kto tu jest idiotą!'},
			{character: 'f', index: 64, text: 'Pewnie zamiast się uczyć, nagrywałeś fanduby.'},
			{character: 'f', index: 65, text: 'Twoja stara musiała zjeść dużo lodu, żeby urodzić takiego bałwana.'},
		]),
		'play sound laughTrack',
		'jump quiz4',
	],

	quiz1: [
		{'Input': {
			'Text': 'Ile to jest 6 razy 7?',
			'Save': function (input) {
				this.storage().player.quiz1 = input === '42';
			}
		},
		},
		{'Conditional': {
			'Condition': function () {
				return this.storage().player.quiz1;
			},
			'True': 'n Dobrze. Chociaż nie masz powodów do zadowolenia, mój dwuletni syn potrafi rozwiązać coś takiego szybciej.',
			'False': 'jump quiz1fail'
		}},
		'play sound laughTrack',
		'show character j jpose2emberrassed at leftish',
		'wait 2500',
		think('j', 44, 'Może jednak nie skończę jako niewidzialny bachor przyprawiający o ból głowy.'),
		think('j', 45, 'Chyba że to była pułapka, żeby osłabić moją czujność.'),
		think('j', 46, 'Wszystko okaże się przy kolejnym pytaniu.'),
		'n Teraz angielski. Coś, co każdy powinien wiedzieć.',
		'n Podaj jedyne poprawne tłumaczenie wyrażenia <i>Legends never die</i>.',
		'show character f fpose2excited at rightish',
		say('f', 66, 'Legendy nigdy nie umierajo!'),
		'...',
		'jump quiz2',
	],
	quiz2: [
		{'Input': {
			'Text': 'Podaj jedyne poprawne tłumaczenie wyrażenia <i>Legends never die</i>.',
			'Save': function (input) {
				this.storage().player.quiz2 = [
					'Legend slychac glos',
					'legend slychac glos',
					'Legend słychać głos',
					'legend słychać głos',
				]
					.includes(input);
			}
		},
		},
		{'Conditional': {
			'Condition': function () {
				return this.storage().player.quiz2;
			},
			'True': 'Odetchnąłem z ulgą.',
			'False': 'jump quiz2fail'
		}},
		'show character j jpose1relieved at leftish',
		'Jednak tajemniczy bogowie krainy potworów spod łóżka postanowili się nade mną zlitować.',
		say('j', 47, 'Komentarze na YouTubie zdecydowanie mówią co innego...'),
		'n No, skoro jesteś taki bystry, to nie powinieneś mieć problemu z kolejnym pytaniem. Zobaczymy, czy po nim też będzie ci do śmiechu.',
		'play sound laughTrack',
		'wait 500',
		'n Historia. Kiedy urodził się pieseł?',
		'show character f fpose1neutral at rightish',
		say('f', 67, 'Wow.'),
		'show character j jpose1bored at leftish',
		think('j', 48, 'To już serio zaczyna być parodia.'),
		think('j', 49, 'Co to w ogóle za szkoła?'),
		think('j', 50, 'Frankie za to bawi się aż za dobrze.'),
		'jump quiz3',
	],
	quiz3: [
		{'Input': {
			'Text': 'Kiedy urodził się pieseł?',
			'Save': function (input) {
				this.storage().player.quiz3 = new Date(input).getTime() === new Date('2005.11.02').getTime()
						|| new Date(input).getTime() === new Date('02.11.2005').getTime();
			}
		},
		},
		{'Conditional': {
			'Condition': function () {
				return this.storage().player.quiz3;
			},
			'True': 	say('j', 48, 'Ale kto by pomyślał, że siedzenie całe życie na reddicie okaże się przydatne w  szkole.'),
			'False': 'jump quiz3fail'
		}},
		'show character j jpose1relieved at leftish',
		'n Pierwszy raz widzę, żebyś był w stanie powiedzieć coś sensownego. Chyba masz dzisiaj szczęśliwy dzień. ',
		'show character f fpose1smileclosedeyes at rightish',
		say('f', 68, 'Najlepszy! W końcu dzisiaj poznałeś mnie!'),
		'n Na koniec geografia. Podaj stolicę Republiki Chińskiej.',
		'show character f fpose1neutral at rightish',
		say('f', 69, 'Pekin!'),
		'show character j jpose3determined at leftish',
		think('j', 51, 'Okej, koniec z memami, ale na to na szczęście też znam odpowiedź!'),
		'jump quiz4'
	],
	quiz4: [
		{'Input': {
			'Text': 'Stolica Republiki Chińskiej to?',
			'Save': function (input) {
				this.storage().player.quiz4 = ['Tajpej', 'Taipei', 'Taipei City', '台北', '臺北'].includes(input);
			}
		},
		},
		{'Conditional': {
			'Condition': function () {
				return this.storage().player.quiz4;
			},
			'True': 'n Tym razem udało ci się coś z siebie wydusić. Byłeś o włos od oblania, ale będę hojna i postawię ci dwóję. Wracaj na miejsce.',
			'False': 'jump quiz4fail'
		}},
		'Nauczycielka szybkim ruchem wyciągnęła w moją stronę kartkę wyrwaną z zeszytu, na której czerwonym długopisem napisane było ogromne „2-”.',
		'show character j jpose1relieved at leftish',
		think('j', 52, 'Zdecydowanie najdziwniejsze odpytywanie w moim życiu...'),
		'play sound schoolBell',
		'stop music creepy',
		'play music enigmatic with loop',
		'hide character n with fadeOut',
		'show character f fpose1cheeky at rightish',
		say('f', 70, 'Tak się grzebałeś, że skończyła się lekcja!'),
		say('f', 71, 'Chodź, czeka na ciebie jeszcze dużo atrakcji!'),
		'show character j jpose1bored at leftish',
		say('j', 49, 'Zabawa, jak na pogrzebie. Mam nadzieję, że to nie jest szczyt twoich możliwości.'),
		say('f', 72, 'Skoro jesteś taki pewny siebie, to na pewno nic cię już nie zaskoczy.'),
		'show character f fpose1creepy at rightish',
		'stop music enigmatic',
		say('f', 73, 'Chociaż bardzo się postaram, żeby tak nie było!'),
		'j ...',
		'show character j jpose1cheeky at leftish',
		'play music enigmatic with loop',
		say('j', 50, 'W sumie w porównaniu do tej nauczycielki, nie jesteś wcale taki straszny.'),
		'show character f fpose1neutral at rightish',
		say('f', 74, '...'),
		'jump chapter4'
	],

	chapter4: [
		'show background hallwaySchoolNight with fadeIn',
		'show character j jpose1thinking at leftish',
		think('j', 53, 'Najpierw pedofil, teraz nauczycielka...'),
		think('j', 54, 'Aż boję się myśleć, co będzie dalej.'),
		think('j', 55, 'I w jaki sposób to wszystko ma mi pomóc się stąd wydostać?'),
		'show character j jpose1annoyed at leftish',
		say('j', 51, 'Mówiłeś, że wydostanę się stąd, jak rozwiążę questy.'),
		say('j', 52, 'Rozwiązałem. Możemy skończyć tę żałosną zabawę?'),
		'show character f fpose2fakecry at rightish',
		say('f', 75, 'Jak to żałosną? Przecież tak się staram...'),
		'show character j jpose1bored at leftish',
		say('j', 53, 'Przez twoje starania znikają moje chęci do życia.'),
		'show character f fpose3neutral at rightish',
		say('f', 76, '...'),
		say('j', 54, 'Następnym razem, jak mnie tu wciągniesz, wymyśl coś lepszego.'),
		'show character f fpose3sad at rightish',
		say('f', 77, 'Ta...'),
		'show character j jpose1surprised at leftish',
		think('j', 56, 'Hm?'),
		think('j', 57, 'Zabrzmiał wyjątkowo smutno...'),
		'...',
		'show background hallwaySchoolDarker50 with fadeIn',
		'...',
		'stop music enigmatic with fade 5',
		'show background hallwaySchoolDarker60 with fadeIn',
		'...',
		'show background hallwaySchoolDarker70 with fadeIn',
		'show character j jpose1neutral at leftish',
		say('j', 55, 'Hej, Frankie, nie sądzisz, że ten korytarz jest jakiś dłuższy niż wcześniej?'),
		'show background hallwaySchoolDarker75 with fadeIn',
		say('j', 56, 'I... ciemniejszy?'),
		'...',
		'show background hallwaySchoolDarker80 with fadeIn',
		'...',
		'play music insects with loop fade 5',
		'hide character f with fadeOut',
		'show character j jpose1surprised at center',
		say('j', 57, 'Frankie?'),
		'show background hallwaySchoolDarker90 with fadeIn',
		'...',
		'show background black with fadeIn',
		'show character j jpose1annoyed at center',
		say('j', 58, 'Frankie, czuję, że mnie macasz po ramieniu.'),
		'...',
		think('j', 58, 'Bo to, co czuję, to Frankie, prawda?'),
		'show character j jpose3veryangryscreamo at center',
		say('j', 59, 'Frankie, spadaj!'),
		'Spróbowałem strzepnąć rękę Frankiego z ramienia...',
		'show character j jpose2scared at center',
		say('j', 60, 'Aaaaaagh!!!'),
		say('j', 61, 'Co to było?!'),
		'show character j jpose2disgusted at center',
		say('j', 62, 'Włochate i obrzydliwe!'),
		'Wyciągnąłem ręce przed siebie, na oślep, w nadzei, że znajdę sylwetkę Frankiego.',
		'show character j jpose3determined at center',
		say('j', 63, 'Frankie!'),
		say('j', 64, 'Gdzie jesteś? To nie jest śmieszne!'),
		'Coś przebiegło mi po nodze i zaczęło się wspinać wyżej.',
		'show character j jpose3veryangryscreamo at center',
		say('j', 65, 'Aaaaagh! Złaźcie ze mnie!'),
		think('j', 59, 'Nie mogę tu tak po prostu stać, muszę się stąd wydostać!'),
		think('j', 60, 'Frankie jak zwykle nie jest pomocny.'),
		'Powstrzymując obrzydzenie, zacząłem iść wzdłuż ściany.',
		'show character j jpose3determined at center',
		'...',
		'Z każdym postawionym krokiem miałem wrażenie, że staję na jakimś żyjącym stworzeniu.',
		'Na skórze czułem delikatne łaskotanie, jakby coraz więcej żyjątek wspinało się po moich rękach i nogach.',
		think('j', 61, 'Tylko mi się wydaje, tylko mi się wydaje!'),
		'Starałem się myśleć o wszystkim, tylko nie o tym, co chodziło mi teraz po ciele.',
		'Przypomniałem sobie, jak wygrałem ogólnoszkolny konkurs z matematyki.',
		'I to, jak razem z siostrą uczyłem się grać na gitarze.',
		'I to, jak świetnie kilka minut temu pocisnąłem Frankiego.',
		'show character j jpose3determinedsmile at center',
		say('j', 66, 'Heh...'),
		'...',
		'...',
		'show character j jpose3determined at center',
		think('j', 62, 'Przecież ten korytarz musi się kiedyś skończyć!'),
		'Nagle moja dłoń trafiła na upragnioną klamkę.',
		'Odetchnąłem z ulgą i nacisnąłem ją najszybciej, jak potrafiłem.',
		'play sound doorhandle',
		'stop music insects',
		'play music psychedelic with loop',
		'show background nightDistrict1 with fadeIn',
		'Zamrugałem, gdy nagle oślepiły mnie światła latarni na ulicy, z której weszliśmy do budynku szkoły.',
		'show character f fpose2angry at rightish with fadeIn',
		'show character j jpose3surprised at leftish',
		say('f', 78, 'No w końcu! Zdążyłem zamówić kebaba, zjeść i wyrzucić folię do kontenera na tworzywa sztuczne, a ciebie nadal nie było.'),
		'show character j jpose3veryangryscreamo at leftish',
		say('j', 67, 'W końcu? W końcu?!'),
		'stop music psychedelic with fade 5',
		say('j', 68, 'Znikasz mi nagle z pola widzenia, zostawiasz mnie zupełnie samego w kompletnych ciemnościach...'),
		say('j', 69, 'A jak w końcu jakimś cudem udaje mi się stamtąd wyjść, to masz czelność narzekać, że długo mi zeszło?!'),
		'show character f fpose3neutral at rightish',
		say('f', 79, 'Chwila, cze-'),
		say('j', 70, 'Skąd miałem wiedzieć, czy coś ci się nie stało, skąd miałem wiedzieć, czy nie zostałem sam?!'),
		say('j', 71, 'Co miałbym zrobić, gdybyś zniknął?'),
		'show character f fpose3emberrassed at rightish',
		say('f', 80, '...'),
		say('f', 81, 'Nie wiedziałem, że aż tak się zezłościsz.'),
		'show character j jpose1annoyed at leftish',
		'j ...',
		say('j', 72, 'Wiesz co, nieważne, zapomnij o tym.'),
		say('j', 73, 'Zróbmy te questy i miejmy to już za sobą.'),
		'show character j jpose1annoyed at leftish',
		say('j', 74, 'Co jest następne?'),
		'show character f fpose2excited at rightish',
		'play music psychedelic with loop',
		say('f', 82, 'A, dobrze, że pytasz! Jestem pewny, że nasz następny przystanek szczególnie ci się spodoba!'),
		say('j', 75, 'Jasne.'),
		'jump chapter5',
	],

	chapter5: [
		'Przez kilka długich minut szliśmy krętymi, ciemnymi uliczkami w niezręcznej ciszy.',
		'Budynki robiły się coraz starsze i bardziej obskurne.',
		'show background street_1Night with fadeIn',
		'show character f fpose1cheeky at rightish',
		'show character j jpose1annoyed at leftish',
		say('f', 83, 'Czujesz się, jak u siebie w domu, co?'),
		say('j', 76, 'Nieśmieszne.'),
		'show character f fpose2angry at rightish',
		say('f', 84, 'Serio dalej jesteś zły? Przecież powiedziałem, że nie chciałem cię rozzłościć.'),
		'j ...',
		say('f', 85, 'No błagam, jak długo jeszcze zamierzasz to ciągnąć?'),
		'j ...',
		'show character f fpose2determined at rightish',
		say('f', 86, 'Dobra, przepraszam, nie chciałem zostawić panicza samego w opałach!'),
		say('f', 87, 'Strasznie, strasznie, strasznie mi przykro, czy teraz możesz przestać się dąsać?'),
		'j ...',
		'show character j jpose1cheeky at leftish',
		say('j', 77, 'Tak bardzo się stęskniłeś za moim głosem?'),
		say('j', 78, 'Wiem, że jestem zniewalający, ale nie sądziłem, że tak szybko się zakochasz.'),
		'show character f fpose3disgusted at rightish',
		say('f', 88, 'Cofam to. Dupek z ciebie.'),
		say('j', 79, 'Co tam mówisz? Nie słyszę, zagłusza cię dźwięk mojej zajebistości.'),
		say('f', 89, '...'),
		'Frankie gwałtownie zatrzymał się przed drzwiami jednego z budynków.',
		'show background houseOldNight with fadeIn',
		'Złapał za klamkę, z rozmachem otworzył drzwi i gestem zaprosił mnie do środka.',
		'Doświadczenie podpowiadało mi, że nie może być to zwyczajna kamienica zamieszkana przez miłą, starszą panią z psem.',
		'Chyba że psem mordercą.',
		'A staruszka pewnie w wolnej chwili dorabiała jako mroczny klaun.',
		'Tak, to brzmiało prawdopodobnie.',
		'stop music psychedelic with fade 3',
		'play sound slidingDoor',
		'Niechętnie podążyłem za Frankiem.',
		'show background black with fadeIn',
		'stop sound slidingDoor',
		'play sound hospitalCorridor',
		'...',
		'show background dentalOffice with fadeIn',
		'stop sound hospitalCorridor',
		'play sound dentalOffice',
		'show character j jpose1neutral at rightis with fadeInh',
		'show character f fpose3disgusted at rightish with fadeIn',
		'j ...',
		say('j', 80, 'Aż boję się zapytać, co mnie tutaj czeka.'),
		'show character f fpose1smileclosedeyes at rightish',
		say('f', 90, 'Chcę się upewnić, że wrócisz do domu ze śnieżnobiałym uśmiechem.'),
		'show character d dpose1neutral at tower with fadeIn',
		'play sound dentalDrill',
		'show character j jpose2scared at leftish',
		'Całe życie, a konkretnie wszystkie wizyty u dentysty przeleciały mi przed oczami. To koniec. Tak umrę.',
		'Odruchowo złapałem Frankiego za ramię.',
		'show character f fpose1cheeky at rightish',
		say('f', 91, 'Aww, przestraszyłeś się? Chcesz się przytulić?'),
		'Szybko zabrałem rękę.',
		'show character j jpose2disgusted at leftish',
		say('j', 81, 'Nigdy w życiu.'),
		'show character f fpose2angry at rightish',
		say('f', 92, 'Czego się tak boisz? Przecież właśnie całkiem sam przeszedłeś przez tunel pełen pająków!'),
		'show character j jpose2scared at leftish',
		say('j', 82, 'To były pająki?!'),
		'show character f fpose2surprised at rightish',
		say('f', 93, 'Co...? Kto to powiedział?'),
		'show character j jpose1thinking at leftish',
		'stop sound dentalOffice with fade 5',
		'play music evolution with fade 15',
		think('j', 63, 'Rzeczywiście, jest w tym trochę racji.'),
		think('j', 64, 'Od kiedy się tu pojawiłem, robiłem dużo strasznych rzeczy.'),
		think('j', 65, 'Może wizyta u dentysty nie jest wcale taka najgorsza?'),
		think('j', 66, 'Może to zwyczajny przegląd?'),
		think('j', 67, 'Ostatnio codziennie używałem nici dentystycznej.'),
		think('j', 68, 'No i nie zjadłem w końcu tego cukierka.'),
		think('j', 69, 'Frankie chyba nie pozwoliłby, żeby stała mi się krzywda, prawda?'),
		'Dentysta wskazał zachęcająco na fotel.',
		'show character j jpose3determined at leftish',
		'Z trudem przełknąłem ślinę i chwiejnym krokiem ruszyłem w jego stronę.',
		'Usiadłem na fotelu i zacisnąłem mocno oczy.',
		'show scene black',
		'play sound dentalDrillLong',
		'wait 14000',
		'show background dentalOffice with fadeIn',
		'show character d dpose1neutral at tower with fadeIn',
		'show character j jpose3determined at leftish with fadeIn',
		'show character f fpose3disgusted at rightish with fadeIn',
		'Kiedy je otworzyłem, było już po wszystkim.',
		'W kącie pokoju zauważyłem Frankiego z twarzą bladą jak ściana.',
		'show character j jpose1bored at leftish',
		say('j', 83, 'Żyjesz tam?'),
		'show character f fpose3sad at rightish',
		say('f', 94, 'Jestem potworem spod łóżka, czy kiedykolwiek w ogóle żyłem?'),
		// rozbawiony Jessie
		'show character j jpose1cheeky at leftish',
		say('j', 84, 'Nie wiem, ale teraz zdecydowanie wyglądasz, jakbyś przestał.'),
		'show character f fpose2determined at rightish',
		say('f', 95, 'To nie tak, że boję się wierteł. Po prostu nie lubię czystych pokoi.'),
		'W międzyczasie dentysta wyjął coś z szuflady biurka i wyciągnął w moją stronę.',
		'show character j jpose1surprised at leftish',
		think('j', 70, 'Naklejka <i>dzielny pacjent</i>?'),
		think('j', 71, 'Takiej jeszcze nie miałem!'),
		'Dentysta pokazał gest kciuka w górę i rozpłynął się w powietrzu.',
		'hide character d with fadeOut',
		'show character f fpose3disgusted at rightish',
		say('f', 96, 'Wyjdźmy stąd w końcu.'),
		'jump chapter6'
	],

	chapter6: [
		'show background houseOldNight with fadeIn',
		'show character j jpose3determinedsmile at leftish with fadeIn',
		'show character f fpose3disgusted at rightish with fadeIn',
		'stop music evolution',
		'play music psychedelic',
		say('j', 85, 'Chyba jestem w tym coraz lepszy!'),
		say('j', 86, 'To co dalej? Jakie jest następne zadanie?'),
		'show character f fpose3sad at rightish',
		say('f', 97, 'Nie ma następnego zadania.'),
		'stop music with fade 5',
		say('f', 98, 'Skończyły mi się pomysły.'),
		'show character j jpose3surprised at leftish',
		'j ...',
		say('j', 87, 'Co?'),
		say('j', 88, 'Co to znaczy, że skończyły ci się pomysły?'),
		say('f', 99, 'Starałem się wymyślać jak najtrudniejsze zadania, ale jestem... coraz bardziej zmęczony.'),
		say('j', 89, 'Wymyślać...? Czy one nie miały mi pomóc się stąd wydostać?'),
		'show character f fpose2excited at rightish',
		say('f', 100, 'Ach, to był tylko taki żart!'),
		'show character f fpose2creepy at rightish',
		say('f', 101, 'To ja rządzę tym miejscem!'),
		say('f', 102, 'To ja cię tu ściągnąłem!'),
		say('f', 103, 'I tylko ja jestem w stanie cię wypuścić!'),
		'show character f fpose1neutral at rightish',
		say('f', 104, 'Tak naprawdę wszystko, co zrobiłeś do tej pory, nie miało znaczenia!'),
		'show character j jpose3veryangryscreamo at leftish',
		'play music november with loop',
		say('j', 90, 'C-co? Robiłem to wszystko... te durne questy... bo miałeś taki kaprys?!'),
		say('j', 91, 'I nadal nie mogę się stąd wydostać?!'),
		say('j', 92, 'Czego jeszcze ode mnie chcesz?'),
		'show character j jpose3crying at leftish',
		say('j', 93, 'Myślałem, że w końcu udało mi się znaleźć przyjaciela...'),
		'show character f fpose3neutral at rightish',
		say('f', 105, '...'),
		say('f', 106, 'No co ty! Nigdy nie planowałem się z tobą przyjaźnić.'),
		'Nie mogłem dłużej na niego patrzeć.',
		'Przygryzłem wargę, siłą woli tłumiąc łzy i zacząłem biec przed siebie.',
		'hide character f with fadeOut',
		'show character j jpose3crying at center',
		'Nie wiedziałem, dokąd biegnę i nie miało to większego znaczenia.',
		'Chciałem tylko znaleźć się jak najdalej od niego.',
		'...',
		'...',
		'Nagle zorientowałem się, że dobiegłem do miejsca, w którym wszystko się zaczęło.',
		'show scene traditionalPavilionNight with fadeIn',
		'show character j jpose3crying at center with fadeIn',
		say('j', 94, '...'),
		think('j', 72, 'Co ja teraz zrobię...?'),
		think('j', 73, 'Nigdy nie zobaczę mamy? Siostry?'),
		'j ...',
		'stop music november with fade 5',
		think('j', 74, 'Czy Frankie mówił prawdę?'),
		think('j', 75, 'To wszystko nie miało znaczenia?'),
		think('j', 76, 'Naprawdę nie widział we mnie przyjaciela?'),
		'show character f fpose3neutral at rightish with fadeIn',
		say('f', 107, 'Nieźle biegasz, jak na takiego nerda.'),
		say('f', 108, '...'),
		'show character j jpose3determined at leftish',
		say('j', 95, 'Po co za mną przylazłeś?'),
		say('f', 109, 'A co zamierzasz tu beze mnie zrobić?'),
		'show character j jpose3veryangrycrying at leftish',
		say('j', 96, 'Nie obchodzi mnie, co mogę zrobić, a co nie!'),
		say('j', 97, 'Po prostu nie chcę cię więcej widzieć!'),
		say('j', 98, 'Nie chcę mieć z tobą nic wspólnego!'),
		say('j', 99, 'Nie potrzebuję cię!'),
		'show character f fpose3serious at rightish',
		say('f', 110, 'Dobrze wiem, że mnie nie potrzebujesz.'),
		say('f', 111, 'Prawda jest taka... że to ja potrzebuję ciebie.'),
		'play music piano_moment with fade 5 with loop',
		'show character j jpose3surprised at leftish',
		say('j', 100, 'Co...?'),
		say('f', 112, 'Dlatego cię tu ściągnąłem.'),
		say('f', 113, 'Potwory spod łóżka żywią się strachem dzieci, które straszą.'),
		'show character f fpose3sad at rightish',
		say('f', 114, 'A ty... przestajesz się mnie bać.'),
		'j ...',
		say('f', 115, 'Zwykle potwory godzą się ze swoim losem i gdy przychodzi ich czas... zwyczajnie znikają.'),
		say('f', 116, 'Ale ja nie chciałem zaakceptować, że mój czas też nadszedł.'),
		say('f', 117, 'Z każdym dniem czułem się słabszy i słabszy.'),
		say('f', 118, 'Pomyślałem, że jeśli cię tu sprowadzę i będę cię straszył... będę mógł żyć trochę dłużej.'),
		say('f', 119, 'Ale to nie zadziałało.'),
		say('f', 120, 'Zamiast bać się coraz bardziej, ty stawałeś się odważniejszy.'),
		say('f', 121, 'A ja... zacząłem cię lubić.'),
		'show character j jpose3crying at leftish',
		say('j', 101, 'Ja też cię lubię, Frankie! Jesteś moim pierwszym przyjacielem!'),
		'show character f fpose3crying at rightish',
		say('f', 122, 'I właśnie w tym problem!'),
		say('f', 123, 'Nie możesz mnie lubić!'),
		say('f', 124, 'Im bardziej mnie lubisz, tym mniej się mnie boisz.'),
		say('f', 125, 'A to sprawia, że... znikam.'),
		say('f', 126, 'Potwór spod łóżka nie może przyjaźnić się ze swoją ofiarą!'),
		'show character j jpose3veryangrycrying at leftish',
		say('j', 102, 'Ale ja nie chcę, żebyś zniknął!'),
		say('j', 103, 'Musi być jakiś sposób, by utrzymać cię przy życiu.'),
		'show character f fpose3serious at rightish',
		say('f', 127, 'Nie ma. Muszę cię odesłać... zanim będzie za późno.'),
		say('f', 128, 'Jeśli zniknę, nie będzie nikogo, kto może cię odesłać i zostaniesz tu na zawsze.'),
		'show character j jpose3crying at leftish',
		say('j', 104, 'Ale ja... nie chcę jeszcze wracać. Nie, jeśli to ma znaczyć, że nigdy więcej cię nie zobaczę!'),
		'show character f fpose3crying at rightish',
		say('f', 129, 'Jeśli zniknę, będziesz się tu błąkał do śmierci.'),
		say('f', 130, 'Jessie... ja też chciałbym spędzić z tobą więcej czasu.'),
		say('f', 131, 'Chciałbym poznać cię w innym świecie...'),
		'j ...',
		say('f', 132, '...'),
		say('j', 105, 'Naprawdę nie da się nic zrobić...?'),
		'show character f fpose3cryingsmile at rightish',
		say('f', 133, 'Chyba ostatecznie nie było aż tak strasznie?'),
		'show character j jpose3cryingsmile at leftish',
		'j ...',
		say('j', 106, 'Słaby potwór z ciebie.'),
		say('f', 134, 'Hihi. Teraz czeka na ciebie ostatni quest.'),
		say('f', 135, 'Musisz zamknąć oczy i policzyć od dziesięciu w tył.'),
		'Spojrzałem na Frankiego po raz ostatni, próbując wyryć sobie w pamięci jego twarz.',
		'Niechętnie zamknąłem oczy i poczułem, jak łzy spływają mi po policzkach.',
		'Zacząłem odliczanie...',
		//znikają obaj
		say('jf', 1, '10... 9... 8...'),
		say('jf', 2, '7... 6... 5...'),
		say('jf', 3, '4... 3... 2...'),
		'show scene black with fadeIn',
		'stop music with fade 1',
		say('j', 111, '1.'),
		'jump epilog'
	],

	epilog: [
		'wait 1000',
		'end'
	]
});
